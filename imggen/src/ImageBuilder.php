<?php /* -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; -*- */ ?>                                   
<?php                                                                                                            

class hyImageBuilder
{
    const DEFAULT_FORMAT = IMG_PNG;

	private $x;
	private $y;
    private $width;
    private $height;

    private $format;
    private $file;

    public function __construct($width, $height, $file = NULL)
    {
		// Default text string offsets
		$this->x = 0;
		$this->y = 0;
		
        // TBD $width, $height > 0
        $this->width = $width;
        $this->height = $height;

        // TBD: Check the file path...
        $this->file = $file;
        $this->format = self::DEFAULT_FORMAT;
    }

    // ....
    private function createImage() { }
    private function destroyImage() { }

    public function getRectangle()
    {
        return array($this->width, $this->height);
    }

    public function setRectangle($width, $height)
    {
        // TBD $width, $height > 0
        $this->width = $width;
        $this->height = $height;
    }
	
	public function setOffsets($x, $y)
	{
		$this->x = $x;
		$this->y = $y;
	}

    public function getFormat()
    {
        return $this->format;
    }
    public function setFormat($format)
    {
        if (ImageTypes() & $format) {
            $this->format = $format;
            return $format;
        } else {
            // log "The format ($format) not supported."
            return false;
        }
    }

    public function getFormatSuffix()
    {
        return self::getFormatName($this->format);
    }

    public function createTextButton($text, $file = NULL)
    {
        // If set, it overwrites the default output file (this time only)
        if(isset($file)) {
            // TBD: Check the file path...
            $outfile = $file;
        } else {
            // use default, if any.
            $outfile = $this->file;
        }
        
        //Create the image resource 
        //$image = ImageCreate($this->width, $this->height);  
        $image = ImageCreateTrueColor($this->width, $this->height);  

        //We are making three colors, white, black and gray
        $white = ImageColorAllocate($image, 255, 255, 255);
        $black = ImageColorAllocate($image, 0, 0, 0);
        $grey = ImageColorAllocate($image, 204, 204, 204);

        //Make the background black 
        ImageFill($image, 0, 0, $black); 

		$font = 3;
        ImageString($image, $font, $this->x, $this->y, $text, $white); 

        ImageRectangle($image, 0, 0, $this->width-1, $this->height-1, $grey); 
        //imageline($image, 0, $height/2, $width, $height/2, $grey); 
        //imageline($image, $width/2, 0, $width/2, $height, $grey); 
 
        //Output the newly created image in jpeg format 
        if($this->format == IMG_GIF) {
            //header("Content-Type: image/gif");
            $suc = ImageGIF($image, $outfile);
        } else if($this->format == IMG_JPEG) {
            //header("Content-Type: image/jpeg");
            $suc = ImageJPEG($image, $outfile);
        } else if($this->format == IMG_PNG) {
            //header("Content-Type: image/png");     
            $suc = ImagePNG($image, $outfile);
        } else if($this->format == IMG_WBMP) {
            //header("Content-Type: image/wbmp");
            $suc = ImageWBMP($image, $outfile);
        } else {
            // This should NOT happen....
            // log "The image format not recognized!."
            $suc = false;
        }

        //Free up resources
        ImageDestroy($image);

        return $suc;
    }


    public static function getFormatName($format_enum)
    {
        if($format_enum == IMG_GIF) {
            return 'gif';
        } else if($format_enum == IMG_JPEG) {
            return 'jpeg';
        } else if($format_enum == IMG_PNG) {
            return 'png';
        } else if($format_enum == IMG_WBMP) {
            return 'wbmp';
        } else {
            // This should NOT happen....
            // ERROR!   log "The image format not recognized!."
            return false;   // ????
        }
    }

    public static function getFormatEnum($format_name)
    {
        if($format_name == 'gif') {
            return IMG_GIF;
        } else if($format_name == 'jpeg') {
            return IMG_JPEG;
        } else if($format_name == 'png') {
            return IMG_PNG;
        } else if($format_name == 'wbmp') {
            return IMG_WBMP;
        } else {
            // This should NOT happen....
            // ERROR!   log "The image format not recognized!."
            return false;   // ????
        }
    }

}
