<?php /* -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; -*- */ ?>                                   
<?php                                                                                                            
require_once 'PHPUnit/Framework.php';
require_once dirname(__FILE__) . '/../src/ImageBuilder.php';


class hyImageBuilderTests extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        echo "[[ Test class: " . __CLASS__ . "\n";
    }

    protected function tearDown()
    {
        echo "]] Test end (" . __CLASS__ . ")\n";
    }

    public function testGetRectangle()
    {
        $width_expected = 100;
        $height_expected = 50;
        $imgBuilder = new hyImageBuilder($width_expected, $height_expected);
        list($width, $height) = $imgBuilder->getRectangle();
        echo "width = $width; height = $height.\n";

        $this->assertEquals($width_expected, $width);
        $this->assertEquals($height_expected, $height);
    }

    public function testGetFormat()
    {
        $width = 100;
        $height = 50;
        $imgBuilder = new hyImageBuilder($width, $height);
        $format = $imgBuilder->getFormat();
        echo "format = $format.\n";

        $this->assertEquals(IMG_PNG, $format);
    }

    public function testSetFormat()
    {
        $width = 100;
        $height = 50;
        $imgBuilder = new hyImageBuilder($width, $height);

        $format_expected = IMG_JPEG;
        $suc = $imgBuilder->setFormat($format_expected);
        $this->assertEquals($format_expected, $suc);

        $format = $imgBuilder->getFormat();
        echo "format ({$imgBuilder->getFormatSuffix()}) = $format.\n";

        $this->assertEquals($format_expected, $format);

        $format_expected = IMG_GIF;
        $suc = $imgBuilder->setFormat($format_expected);
        $this->assertEquals($format_expected, $suc);

        $format = $imgBuilder->getFormat();
        echo "format ({$imgBuilder->getFormatSuffix()}) = $format.\n";

        $this->assertEquals($format_expected, $format);

        $format_expected = IMG_WBMP;
        $suc = $imgBuilder->setFormat($format_expected);
        $this->assertEquals($format_expected, $suc);

        $format = $imgBuilder->getFormat();
        echo "format ({$imgBuilder->getFormatSuffix()}) = $format.\n";

        $this->assertEquals($format_expected, $format);

        $format_expected = IMG_PNG;
        $suc = $imgBuilder->setFormat($format_expected);
        $this->assertEquals($format_expected, $suc);

        $format = $imgBuilder->getFormat();
        echo "format ({$imgBuilder->getFormatSuffix()}) = $format.\n";

        $this->assertEquals($format_expected, $format);
    }


    public function testCreateTextButton()
    {
        $width = 99;
        $height = 22;

        $file = '/home/hyoon/projects/webprojects/lib/tools/imggen/test/testimage000.png';
        echo "file = $file.\n";

        $imgBuilder = new hyImageBuilder($width, $height, $file);

        //$imgfile = NULL;
        //$imgfile = 'testimage.png';
        $imgfile = '/home/hyoon/projects/webprojects/lib/tools/imggen/test/testimage_' . date("Ymd") . '.' . $imgBuilder->getFormatSuffix();
        echo "imgfile = $imgfile.\n";

        $text = "Memobay";
        $suc = $imgBuilder->createTextButton($text, $imgfile);
        $this->assertTrue($suc);
    }


}
