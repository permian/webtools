<?php /* -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; -*- */ ?>                                   
<?php
require_once dirname(__FILE__) . '/../src/ImageBuilder.php';
require_once 'Console/Getopt.php';

/*
$shortopts  = "w:h:t:f::";
//$longopts  = array("width:", "height:", "text:", "format::");
//$options = getopt($shortopts, $longopts);
$options = getopt($shortopts);
//var_dump($options);

$width = intval($options["w"]);
if(!$width) {
    exit("Width is not an integer.\n");
}
$height = intval($options["h"]);
if(!$height) {
    exit("Height is not an integer.\n");
}
$text = strval($options["t"]);
if(!isset($text) || empty($text)) {
    exit("Text arg is not set.\n");
}
if(isset($options["f"])) {
    $format = hyImageBuilder::getFormatEnum($options["f"]);
}
if(!isset($format) || !$format) {
    $format = hyImageBuilder::DEFAULT_FORMAT;
}
*/

$shortopts  = "w:h:t:x::y::f::";
//$longopts  = array("width=", "height=", "text=", "x==", "y==", "format==");
$cgo  = new Console_Getopt();
$args = $cgo->readPHPArgv();
array_shift($args);
$options = $cgo->getopt2($args, $shortopts);
//var_dump($options);

$x = 0;
$y = 0;
foreach($options[0] as $olist) {
	$key = $olist[0];
	$val = $olist[1];
	echo "$key:$val.\n";
	if($key == 'w') {
		$width = intval($val);
	} else if($key == 'h') {
		$height = intval($val);
	} else if($key == 'x') {
		$x = intval($val);
	} else if($key == 'y') {
		$y = intval($val);
	} else if($key == 't') {
		$text = $val;
	} else if($key == 'f') {
		$format = hyImageBuilder::getFormatEnum($val);
	}
}

if(!$width) {
    exit("Width is not an integer.\n");
}
if(!$height) {
    exit("Height is not an integer.\n");
}
if(!isset($text) || empty($text)) {
    exit("Text arg is not set.\n");
}
if(!isset($format) || !$format) {
    $format = hyImageBuilder::DEFAULT_FORMAT;
}

// Image builder
$imgBuilder = new hyImageBuilder($width, $height);
$imgBuilder->setFormat($format);
$imgBuilder->setOffsets($x, $y);

// temporary
$imgfile = dirname(__FILE__) . '/testimage_' . date("Ymd") . '.' . $imgBuilder->getFormatSuffix();
echo "imgfile = $imgfile.\n";

$suc = $imgBuilder->createTextButton($text, $imgfile);
echo "suc = $suc.\n";
